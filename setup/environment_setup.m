%% Map parameters

x_min = -1e5; % ~ -100km longitude / 
x_max = 3e5; % ~ +100km longitude / 300km longitude
% x_min = -1.7e7; % ~ +180deg longitude / 
% x_max = 1.7e7; % ~ +180deg longitude
x = linspace(x_min,x_max,15);

y_min = -1e5; % -100km latitude
y_max = 1e5; % +100km latitude
% y_min = -4.0e6; % ~ -20deg latitude
% y_max = 4.0e6; % ~ +20deg latitude
y = linspace(y_min,y_max,10);

z_min = 5.5e4; % ~ 55km altitude, 100% buoyant
z_max = 7.0e4; % ~ 70km altitude, 10% buoyant, 90% lift
z = linspace(z_min,z_max,10);

limits = [x_min, x_max, y_min, y_max, z_min, z_max];

[X,Y,Z] = meshgrid(x,y,z);

[U, V, W] = wind_field(X,Y,Z, hps.wind_type, wind_magnitude);

f = gcf;

% f.Units = 'normalized';
% f.OuterPosition = [0 0 1 1];
quiver3(X,Y,Z,U,V,W,0.5,'Autoscale','on');
hold on