%% Create Aircraft object

aircraft_properties.mass = 4.5e2;
aircraft_properties.volume = aircraft_properties.mass / air_density(z_min); %% 100% buoyancy at 55km 
aircraft_properties.wing_chord = 10;
aircraft_properties.wing_span = 50;
aircraft_properties.wing_area = aircraft_properties.wing_chord * aircraft_properties.wing_span;
aircraft_properties.wing_aspect_ratio = aircraft_properties.wing_span ^2 / aircraft_properties.wing_area;
aircraft_properties.wing_efficiency = 0.85;
aircraft_properties.solar_panel_area = 50;
aircraft_properties.ci = [0.02, 0, 1/(pi* aircraft_properties.wing_aspect_ratio * aircraft_properties.wing_efficiency)];
% aircraft_properties.ci = [0.0194, -0.0624, 0.2397, -0.3164, 0.1723];
aircraft_properties.max_roll_angle = deg2rad(30);
aircraft_properties.flight_path_angle_limits = deg2rad([-45, 45]);
aircraft_properties.max_airspeed = max_airspeed; %m/s
aircraft_properties.efficiency_propeller = 0.8;
aircraft_properties.efficiency_shaft = 0.8;
aircraft_properties.battery_max_energy = 5e7; %J
aircraft_properties.battery_min_energy = 0;

initial_state.position = start(1:3);
initial_state.altitude = start(3);
initial_state.heading = 0;
initial_state.airspeed = aircraft_properties.max_airspeed;
initial_state.time = 0;
initial_state.battery_energy = 1e9;
initial_state.kinetic_energy = 0.5 * aircraft_properties.mass * initial_state.airspeed ^2;
initial_state.potential_energy = aircraft_properties.mass * initial_state.altitude * gravity(initial_state.altitude);

%% DO NOT USE BELOW
environment_properties.air_density = air_density(initial_state.altitude);
environment_properties.gravity = gravity(initial_state.altitude);

planning_parameters.integration_time = 120;
planning_parameters.input_T = [0, 10];
planning_parameters.input_airspeed = [20, 30];
planning_parameters.input_delta_heading = deg2rad(-50:10:50);
planning_parameters.input_bank_angle =  deg2rad([0, 30]);
planning_parameters.start = start;
planning_parameters.goal = goal;
planning_parameters.wind_type = wind_type;
planning_parameters.wind_magnitude = wind_magnitude;
planning_parameters.limits = limits;

%%
vamp = aircraft(aircraft_properties, initial_state, environment_properties, planning_parameters);
