%% Map parameters

x_min = -5e4; % ~ -50km longitude / 
x_max = 5e4; % ~ +50km longitude
% x_min = -1.7e7; % ~ +180deg longitude / 
% x_max = 1.7e7; % ~ +180deg longitude
x = linspace(x_min,x_max,10);

y_min = -5e4; % -50km latitude
y_max = 5e4; % +50km latitude
% y_min = 4.0e6; % ~ -20deg latitude
% y_max = 4.0e6; % ~ +20deg latitude
y = linspace(y_min,y_max,10);

z_min = 5.5e4; % ~ 55km altitude, 100% buoyant
z_max = 7.0e4; % ~ 55km altitude, 100% buoyant
z = linspace(z_min,z_max,10);

limits = [x_min, x_max, y_min, y_max, z_min, z_max];

%%
[X,Y,Z] = meshgrid(x,y,z);

% Wind field
[U, V, W] = wind_field(X,Y,Z);

figure('units','normalized','outerposition',[0 0 1 1]);
quiver3(X,Y,Z,U,V,W,0.5,'Autoscale','on');
hold on;
view(-20,85);

%% Set start and goal positions
start = [0, 0, 6e4];
goal = [4e4, 1e4, 6e4];