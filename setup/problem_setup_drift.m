%% Map parameters

x_min = -5e2; % ~ -100km longitude / 
x_max = 1.2e3; % ~ +100km longitude
% x_min = -1.7e7; % ~ +180deg longitude / 
% x_max = 1.7e7; % ~ +180deg longitude
x = linspace(x_min,x_max,15);

y_min = -1e2; % -100km latitude
y_max = 8e2; % +100km latitude
% y_min = 4.0e6; % ~ -20deg latitude
% y_max = 4.0e6; % ~ +20deg latitude
y = linspace(y_min,y_max,10);

z_min = 0; % ~ 55km altitude, 100% buoyant
z_max = 1e3; % ~ 70km altitude, 10% buoyant, 90% lift
z = linspace(z_min,z_max,10);

limits = [x_min, x_max, y_min, y_max, z_min, z_max];

%%
[X,Y,Z] = meshgrid(x,y,z);

% Wind field
[U, V, W] = wind_field(X,Y,Z, wind_type, wind_magnitude);

figure%('units','normalized','outerposition',[0 0 1 1]);
% quiver3(X,Y,Z,U,V,W,0.5,'Autoscale','on');
hold on;
% view(-20,85);

%%

plot3(start(1),start(2),start(3),'g.','MarkerSize',20);
% text(start(1),start(2),start(3),['Start:',num2str(start(1:3))]);
% 
plot3(goal(1),goal(2),goal(3),'r.','MarkerSize',20);
% text(goal(1),goal(2),goal(3),['Goal:',num2str(goal(1:3))], 'HorizontalAlignment','right');
hold on;