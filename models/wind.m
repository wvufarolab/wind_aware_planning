function [u,v,w] = wind(x,y,z)
%Calculate wind field, u,v,w
% https://www.omnicalculator.com/everyday-life/crosswind

% crosswind speed
% wind_speed*sin(a)

% head or tail wind 
% wind_speed*cos(a)

% https://agupubs.onlinelibrary.wiley.com/doi/full/10.1002/2015JE004958
% https://earth-planets-space.springeropen.com/articles/10.1186/s40623-017-0775-3

X = []; % lat
Y = []; % lon
Z = []; % alt
Wx = []; % lat x lon x alt
Wy = []; % lat x lon x alt
Wz = []; % lat x lon x alt

u = interp3(X,Y,Z,Wx,x,y,z);
v = interp3(X,Y,Z,Wy,x,y,z);
w = interp3(X,Y,Z,Wz,x,y,z);

end

