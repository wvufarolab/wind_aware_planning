function [power] = solar_intensity(z)
%SOLARPOWER Summary of this function goes here
%   Detailed explanation goes here
altitudes = [0, 5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000, 55000, 60000, 70000, 75000];
powers = [8.70, 22.0, 37.4, 45.6, 58.5, 71.9, 85.1, 99.0, 112.2, 181.4, 256.0, 404.4, 559.2, 560, 560];

power=interp1(altitudes,powers,z,'linear');

end


