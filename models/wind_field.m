function [u, v, w] = wind_field(x, y, z, wind_type, wind_magnitude)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

U = 0;
V = 0;
W = 0;

if strcmp(wind_type,'venus')
    U = 100 * (1e5-z)/(1e5-6.5e4);
    U (z < 6.5e4) = z(z < 6.5e4)/6.5e4 * 1e2;
    V = 5 * sign(y) .* (z-5.7e4) / (7.0e4-5.7e4); % https://sci.esa.int/web/venus-express/-/43424-wind-speeds-in-venus-s-cloud-layers
    W = 5 - 5 * abs(z-5.7e4)/(7.0e4-5.7e4);
elseif strcmp(wind_type,'constant')
    U = wind_magnitude;
elseif strcmp(wind_type,'upAndDown')
    U = wind_magnitude;
    W = (2 ./ (1+ exp(y./10000))-1) .* 5;
end

u = U.*ones(size(z));
v = V.*ones(size(y));
w = W.*ones(size(x));

end