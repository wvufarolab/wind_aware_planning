function [temperature] = air_temperature(z)

altitudes = [0,5000,10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000,65000,70000,80000,90000,100000];
temperatures = [462,424,385,348,306,264,222,180,143,110,75,27,-10,-30,-43,-76,-104,-112];

temperature=interp1(altitudes,temperatures,z,'linear');

end