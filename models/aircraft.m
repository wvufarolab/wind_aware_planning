classdef aircraft
    %AIRCRAFT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        aircraft_properties
        state
        environment_properties
        planning_parameters
    end
    
    methods
        function obj = aircraft(aircraft_properties, initial_state, environment_properties, planning_parameters)
            %AIRCRAFT Construct an instance of this class
            %   Detailed explanation goes here
            if(nargin==4)
                obj.aircraft_properties = aircraft_properties;
                obj.state = initial_state;
                obj.environment_properties = environment_properties;
                obj.planning_parameters = planning_parameters;
                obj.state.kinetic_energy = 0.5 * obj.aircraft_properties.mass * obj.state.airspeed^2;
                obj.state.potential_energy = obj.aircraft_properties.mass * obj.environment_properties.gravity * obj.state.position(3);
            end
        end
        
        function KE = getKineticEnergy(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            KE = 0.5 * obj.aircraft_properties.mass * obj.state.airspeed^2;
        end
        
        function altitude = getAltitude(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            altitude = obj.state.position(3);
        end
        
        function PE = getPotentialEnergy(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            PE = obj.aircraft_properties.mass * obj.environment_properties.gravity * obj.state.position(3);
        end
        
        function rotation_matrix = getRotationMatrix(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            rotation_matrix = [ cos(obj.state.heading)   sin(obj.state.heading)	0;
                               -sin(obj.state.heading)   cos(obj.state.heading)	0;
                                0                        0                      1];
        end
        
        function cL = lift_coeff(obj, input)
            %UNTITLED6 Summary of this function goes here
            %   Detailed explanation goes here
            cL = 2*obj.aircraft_properties.mass*obj.environment_properties.gravity / (obj.environment_properties.air_density * input.airspeed^2 * obj.aircraft_properties.wing_area * cos(input.bank_angle));
        end
        
        function cD = drag_coeff(obj, cL)
            %UNTITLED6 Summary of this function goes here
            %   Detailed explanation goes here
            
            cD = 0;
            for i=1:length(obj.aircraft_properties.ci)
                cD = cD + obj.aircraft_properties.ci(i)*cL^(i-1);
            end
        end
        
        
        
        function gamma = flight_path_angle(obj, input, cD)
            %UNTITLED6 Summary of this function goes here
            %   Detailed explanation goes here
            gamma = obj.environment_properties.air_density * input.airspeed^2 * obj.aircraft_properties.wing_area * cD/(2*obj.aircraft_properties.mass*obj.environment_properties.gravity) - input.T/(obj.aircraft_properties.mass * obj.environment_properties.gravity);
        end

        function [obj, traj, cost] = getNextState(obj, input, dt, goal, update)

            Ei = obj.getKinematicEnergy(obj.state.position, obj.state.airspeed);
            t = obj.state.time:dt:obj.state.time+obj.planning_parameters.integration_time;

            T = obj.getRotationMatrix;

            cL = obj.lift_coeff(input);
            cD = obj.drag_coeff(cL); % L/D max = 25

            gamma = -obj.flight_path_angle(input, cD);

            % Initialize our state
            current_position = obj.state.position;
            delta_altitude = 0;
            if (obj.state.airspeed ~= input.airspeed)
                delta_altitude = (obj.state.airspeed^2-input.airspeed^2)/(2*obj.environment_properties.gravity);
            end
            current_position(3) = current_position(3) + delta_altitude;

            % 
            heading_dot = 0;

            if (input.bank_angle ~= 0)
                heading_dot = obj.environment_properties.gravity * tan(input.bank_angle) / input.airspeed;
                current_heading = obj.state.heading;
            else
                current_heading = obj.state.heading + input.delta_heading;
            end

            traj = [];
            Ec = 0;
            for i=1:length(t)
                X_dot = [input.airspeed*cos(gamma)*cos(input.delta_heading)
                           input.airspeed*cos(gamma)*sin(input.delta_heading)
                           input.airspeed*sin(gamma)];
                current_heading = current_heading + heading_dot * dt;
                current_position = current_position + ((T*X_dot)' + wind_field(current_position(1),current_position(2),current_position(3))) * dt;
                Ec = Ec + solar_panel_model(current_position(3));
                traj = [traj; current_position];
            end
            Ef = obj.getKinematicEnergy(current_position, input.airspeed);

            Ek = -(Ef - Ei);
            Ep = input.T*input.airspeed*obj.planning_parameters.integration_time;

            dE = (Ek - Ec + Ep);
            dist_to_goal = norm(current_position - goal);
            
            cost = dE/dist_to_goal;

            if (update)
                obj.state.position = current_position;
                obj.state.heading = current_heading;
                obj.state.airspeed = input.airspeed;
                obj.state.time = obj.state.time + obj.planning_parameters.integration_time;
                obj.state.kinematic_energy = Ef;
%                 disp('Got here');
            end
        end            
            
        function position = getPosition(obj, position, input, t, dt)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            T = obj.getRotationMatrix;

            cL = obj.lift_coeff(input);
            cD = obj.drag_coeff(cL); % L/D max = 25

            gamma = -obj.flight_path_angle(input, cD);
            
            if (input.bank_angle == 0)
                X_dot = [input.airspeed*cos(gamma)*cos(obj.state.heading + input.delta_heading)
                           input.airspeed*cos(gamma)*sin(obj.state.heading + input.delta_heading)
                           input.airspeed*sin(gamma)];

                position =  position + ((T*X_dot)' + wind_field(position(1),position(2),position(3))) * dt; % + [0,0,delta_altitude];
            else
                
                curr_heading = obj.state.heading + obj.environment_properties.gravity * tan(input.bank_angle) / input.airspeed * t;

                X_dot = [input.airspeed*cos(gamma)*cos(curr_heading);
                           input.airspeed*cos(gamma)*sin(curr_heading);
                           input.airspeed*sin(gamma)];
                                       
                position = position + ((T*X_dot)' + wind_field(position(1),position(2),position(3))) * dt;% + [0,0,delta_altitude];
                
            end
        end
        
        function [] = plotPrimitives(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            dt = 1;
            wind = [5 1 1];
            figure
            
            for i=1:length(obj.planning_parameters.obj.input_T)
                for j=1:length(obj.planning_parameters.obj.input_airspeed)
                    for k=1:length(obj.planning_parameters.obj.input_delta_heading)
                        for l=1:length(obj.planning_parameters.obj.input_bank_angle)
                            
                            input.T = obj.planning_parameters.obj.input_T(i);
                            input.airspeed = obj.planning_parameters.obj.input_airspeed(j);
                            input.delta_heading = obj.planning_parameters.obj.input_delta_heading(k);
                            input.bank_angle = obj.planning_parameters.obj.input_bank_angle(l);
                            
                            P = obj.state.position;
                            Ps = obj.state.position;
                            
                            for t = obj.state.time:dt:obj.state.time+obj.planning_parameters.integration_time
                                P = obj.getPosition(P, input, wind, t, dt);
                                Ps = [Ps; P];
                            end

                            plot3(Ps(:,1),Ps(:,2),Ps(:,3));
                            hold on
                        end
                    end
                end
            end
        end
    end
end

