function [thrust] = thrust(aircraft_properties, path, gamma, roll)
%THRUST Summary of this function goes here
%   Detailed explanation goes here

    m = aircraft_properties.mass;
    V = aircraft_properties.volume;
    rho = air_density(path(:,3));
    g = gravity(path(:,3)); 
    v_a = aircraft_properties.max_airspeed;
    S_w = aircraft_properties.wing_area;
    c_i = aircraft_properties.ci;
    
    B =  rho .* V .* g;
    W =  m .* g;

    L = (W - B) ./ cos(roll) .* cos(gamma);
    c_L = L ./ (0.5 .* rho .*  v_a .^2 .* S_w);
    
    c_D = c_i(1) + c_i(2) * c_L + c_i(3) * c_L .^ 2;
    
    if (c_D > 2) 
        disp('Unrealistic drag coefficient.');
        c_D = 2;
    end
    
    D =  (0.5 .* rho .*  v_a .^2 .* S_w) .* c_D;
    
    thrust =  D - (W - B) .* sin(gamma);
%     disp(['Thrust: ', num2str(thrust(1,:))]);
    thrust(thrust<0) = 0; 
end