function [B] = buoyancy(z)
%Calculate buoyancy force
%  https://www.youtube.com/watch?v=xh6V4sEeUYs

den = air_density(z); %[g/L]=[Kg/m^3]
mass = 900; %VAMP's mass [kg]
%R=8.31451;  % specific gas constant [J/mol*K]
%M=0.04344;  % molar mass (air: CO2 96.5% 2 3.5%) [Kg/mol]
V = mass / air_density(50000); %volume of VAMP [m^3] %buoyancy gas: hydrogen
g = 8.87;     % gravity [m/s^2]
% T in Kelvin
% P in Pascals

% P=-1.113E-12*z^7+5.817E-10*z^6-1.452E-07*z^5+2.285E-05*z^4-0.002382*z^3+0.1569*z^2-5.825*z+92.11;
% T=8.836E-11*z^6-1.757E-07*z^5+3.527E-05*z^4-0.001936*z^3+0.02895*z^2-7.876*z+462.5;
% 
% T=T+273.17; % temp in K
% P=P*101325; % pres in Pascals

% Density of air (we don't consider humidity for now since it is less than 0.1%)
% consider this dry air to calculate density

% den=(P*M)/(R*T); %[kg/m^3]

% call function instead

B=den*V*g; %Buoyant Force [N]

end

