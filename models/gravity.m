function [g] = gravity(z)
%Calculate gravity on Venus depending on altitude
G=6.67408E-11; % gravitational constant [m^3/(Kg*s^2)]
M=4.867E24;    % mass of Venus [Kg]
r=6.0518E6;    % radius of Venus [m]

g = (G*M)./((r+z).^2); % gravity of Venus at altitude z

end

