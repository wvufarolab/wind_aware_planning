function v_rand = sampleFreeDubinsAirplane(S, vamp, limits)

xDim = limits(2)- limits(1);
yDim = limits(4)- limits(3);
zDim = limits(6)- limits(5);

xOffSet = limits(1);
yOffSet = limits(3);
zOffSet = limits(5);

% Sampling random position and heading
x = rand() * xDim + xOffSet;
y = rand() * yDim + yOffSet;
z = rand() * zDim + zOffSet;
theta = rand * (2*pi); 

vamp.state.position = [x, y, z];
vamp.state.heading = theta;
vamp.state.altitude = vamp.getAltitude();
vamp.state.kinetic_energy = vamp.getKineticEnergy();
vamp.state.potential_energy = vamp.getPotentialEnergy();

v_rand = vertex(S.get_next_idx(), vamp, 0, 0, 0, [], [], []);

end

    