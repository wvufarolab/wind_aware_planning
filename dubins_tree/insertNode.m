function [S, V, E] = insertNode(S, V, E, v_parent, v_child)

% Add the start node the container
S.add_element(v_child);

% Add the start node to the vertex set
V.add_vertex(v_child.idx);

if ~isempty(v_parent)
    % Add edge from v_parent to v_child
    E.add_edge(v_parent.idx, v_child.idx);

    % Add v_child to the children set of v_parent
    S.container(v_parent.idx).children_set = [S.container(v_parent.idx).children_set, v_child.idx];
end

end