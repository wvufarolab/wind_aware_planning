%% Plot Start and Goal
plot3(start(1),start(2),start(3),'o','MarkerSize',5,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','g');
hold on
plot3(goal(1),goal(2),goal(3),'o','MarkerSize',5,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','r');
hold on

%% Plot Graph
for j=2:S.num_elements
    v = S.get_element(j);
    p_init = v.state.state.position;
    v_parent = S.get_element(v.parent_idx);
    p_goal = v_parent.state.state.position;
    if (v.cost_from_start ~= Inf)
        if (flags.plot_battery == true)
            b = v.state.state.battery_energy;
            b_max = v.state.aircraft_properties.battery_max_energy;
            b_hat = (b_max - b)/b_max;
            color = [b_hat 1-b_hat 0];
        elseif (flags.plot_cost == true)
            color = [c_hat 1-c_hat 0];
        else
            color = [0,0,0];
        end
        plot3([p_init(1) p_goal(1)], ...
            [p_init(2), p_goal(2)], ...
            [p_init(3),p_goal(3)], '-','Color',color);
            hold on
    end
end

%% Plot Solution Trajectories
if (flags.plot_min_dist_traj == true)
    plot3(min_dist.v.traj_from_start(:,1),min_dist.v.traj_from_start(:,2),min_dist.v.traj_from_start(:,3),'b-','LineWidth',3);
    hold on
end

if (flags.plot_min_cost_traj == true)
    plot3(min_cost.v.traj_from_start(:,1),min_cost.v.traj_from_start(:,2),min_cost.v.traj_from_start(:,3),'b-','LineWidth',3);
    hold on
end

%%
axis equal
hold on
axis(limits)
% view(3)
xlabel('x [m]','FontSize',20)
ylabel('y [m]','FontSize',20)
zlabel('z [m]','FontSize',20)