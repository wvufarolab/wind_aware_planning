function [S, V, E] = rewire(S, V, E, V_near, v_min, v_new, method)

obstacle_free = true;

for i=1:length(V_near)
    v_near = V_near(i);
    if (v_near.idx == 1 || v_near.idx == v_min.idx)
        continue
    end
    v_near_prime = exactSteerDubinsAirplane(v_new, v_near, method);
    if (obstacle_free == true && v_near_prime.cost_from_start < v_near.cost_from_start) % add second condition
        [S, V, E] = reconnect(S, V, E, v_new, v_near, v_near_prime);
    end
end

end