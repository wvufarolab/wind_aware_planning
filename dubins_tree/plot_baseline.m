%% Plot Wind Field
[U, V, W] = wind_field(X,Y,Z, hps.wind_type, wind_magnitude);

f = gcf;

f.Units = 'normalized';
f.OuterPosition = [0 0 1 1];
quiver3(X,Y,Z,U,V,W,0.5,'Autoscale','on');
hold on

%% Plot Start and Goal
plot3(start(1),start(2),start(3),'o',...
    'MarkerSize',15,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','g');
plot3(goal(1),goal(2),goal(3),'o','MarkerSize',15,...
    'MarkerSize',15,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor','r');
hold on

%%

axis equal
hold on
axis(limits)
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')