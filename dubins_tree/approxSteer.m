function v_new = approxSteer(v_nearest, v_rand, step_size, limits, flags)

xDim = limits(2)- limits(1);
yDim = limits(4)- limits(3);
zDim = limits(6)- limits(5);

xOffSet = limits(1);
yOffSet = limits(3);
zOffSet = limits(5);

p_rand = v_rand.state.state.position;
p_init = v_nearest.state.state.position;

RAND = (p_rand - p_init);
% RAND = (p_rand - p_init) ./ [xDim, yDim, zDim];
% norm(RAND / norm(RAND) * step_size)
p_new = p_init + RAND / norm(RAND) * step_size;

if p_new(3) > limits(6)
    p_new(3) = limits(6);
elseif p_new(3) < limits(5)
    p_new(3) = limits(5);
end

% Bring the vertex closer (using step size) and heading in the direction of
% the stright line connecting the nearest to the random vertices
v_rand.state.state.position = p_new;
v_rand.state.state.heading = atan2(p_new(2)-p_init(2),p_new(1)-p_init(1));
v_rand.state.state.altitude = v_rand.state.getAltitude();
v_rand.state.state.kinetic_energy = v_rand.state.getKineticEnergy();
v_rand.state.state.potential_energy = v_rand.state.getPotentialEnergy();

v_new = v_rand;

if (flags.plot_samples == true)
    plot3(p_new(1), p_new(2), p_new(3),'.','MarkerSize',10);
    hold on
end

end
