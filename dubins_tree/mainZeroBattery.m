%%Trajectory Planning using Dubins' Airplane
clear
close all

addpath('./dubins_tree');
addpath('./figures');
addpath('./graph');
addpath('./models');
addpath('./results');
addpath('./setup');

%% Hyperparameters
hps.wind_type = 'venus';
hps.winds       = [90];
hps.speeds      = [30];
hps.step_sizes  = [1e4];
hps.starts = [-1e5, 0, 5.5e4, 0];
hps.goals = [1e5, 1e5, 5.5e4, 0];
% hps.starts = [-1.7e7, -4e6, 5.5e4, 0];
% hps.goals = [1.7e7, 4e6, 5.5e4, 0];
hps.n_samples = 1000;
hps.max_radius = 1e5;
hps.d_thres = 1e3;

flags.save = false;
flags.plot_samples = true;
flags.plot_battery = true;
flags.plot_cost = false;
flags.plot_min_dist_traj = false;
flags.plot_min_cost_traj = false;
flags.plot_side_views = false;

% hps.winds       = [0, 0, 30, 30, 60, 60, 90, 90];
% hps.speeds      = [30, 30, 30, 30, 30, 30, 30, 30];
% hps.step_sizes  = [1e4, 5e4,  1e4, 5e4, 1e4, 5e4, 1e4, 5e4];
% hps.starts = repmat([-5e4, 0e4, 7e4, 0],8,1);
% hps.goals = repmat([1e5, 4e4, 7e4, 0],8,1);
hps.zero_battery_time = 600;

for i=1:length(hps.winds)
    close all
    clearvars -except i hps flags

    %% Setup environment, aircraft and problem
    wind_type = hps.wind_type;
    wind_magnitude = hps.winds(i);
    max_airspeed = hps.speeds(i);
    step_size = hps.step_sizes(i);
    start = hps.starts(i,:);
    goal = hps.goals(i,:);
    zero_battery_time = hps.zero_battery_time;

    environment_setup;
    aircraft_setup;
    
    %% Motion Planner
    obstacle_free = true;
    method = 'energyOpp';
    methodZB = [method,'ZB'];
    
    % Initialize graph
    [S, V, E] = initializeTree();
    % Create first Vertex object
    v_init = vertex(S.get_next_idx(), vamp, 0, 0, 0, [], [], []);
    % Insert first node to graph
    [S, V, E] = insertNode(S, V, E, [], v_init);
    %%
    % Repeat RRT* algorithm for n samples
    for j=1:hps.n_samples
        % Generate random sample
        v_rand = sampleFreeDubinsAirplane(S, vamp, limits);
        V_zero = zeroBatterySet(S);
        if (isempty(V_zero) || rand > 0.9)
            % Get the nearest vertex
            v_nearest = nearest(S, v_rand);
            % Steer to nearest vertex
            v_new = approxSteer(v_nearest, v_rand, step_size, limits, flags);
            if (obstacle_free == true)
                % Get the near vertices set
                V_near = near(S, v_new, S.num_elements, hps.max_radius, limits);
                % Choose minimum cost parent to connect
                [v_min, v_new] = chooseParent(V_near, v_nearest, v_new, method);
                % Insert node to graph
                [S, V, E] = insertNode(S, V, E, v_min, v_new);
            end
        else
            % Select random parent
            v_parent_ZB = randsample(V_zero,1);
            % Steer zero battery vertex (i.e. drift with wind)
            v_child_ZB = exactSteerZeroBattery(v_parent_ZB, v_rand, methodZB, zero_battery_time);
            % Insert node to graph
            [S, V, E] = insertNode(S, V, E, v_parent_ZB, v_child_ZB);
        end
        
        % Stop search if close enough
        if norm(v_min.state.state.position-v_min.state.planning_parameters.goal(1:3)) < hps.d_thres
            break
        end
        str = ['Iteration: ', int2str(j), '/' ,int2str(hps.n_samples), '.'];
        disp(str);
    end

    %%
    dist = zeros(1,S.num_elements);
    cost = zeros(1,S.num_elements);
    for j=1:S.num_elements
        v = S.get_element(j);
        cost(j) = v.cost_from_start;
        dist(j) = norm(v.state.state.position - v.state.planning_parameters.goal(1:3));
    end
    
    % Vertex at the minimum distance
    [d_min, id] = min(dist);
    min_dist.id = id;
    min_dist.dist = d_min;
    min_dist.v = S.get_element(min_dist.id);
    min_dist.battery  =  min_dist.v.state.state.battery_energy;
    min_dist.t_min =  min_dist.v.state.state.time;
    min_dist.cost =  min_dist.v.cost_from_start;

    % Vertex at the within distance of minimum cost
    cost(dist>5*hps.d_thres) = Inf;
    [c_min, id] = min(cost);
    min_cost.id = id;
    min_cost.cost = c_min;
    min_cost.v = S.get_element(min_cost.id);
    min_cost.battery =  min_cost.v.state.state.battery_energy;
    min_cost.t_min =  min_cost.v.state.state.time;
    min_cost.dist =  dist(min_cost.id);
    
    %% Plotting results
    plot_results;

    %% Saving results
    filename = [hps.wind_type,'_ws_',num2str(wind_magnitude),...\
        '_as_',num2str(max_airspeed),...
        '_ss_',num2str(step_size)];

    if (flags.save == true)
        % Save .mat file
        save(['results/mat/',filename],'hps',...
            'aircraft_properties',...
            'planning_parameters',...
            'environment_properties',...
            'initial_state',...
            'min_dist',...
            'min_cost')
        
        f.CurrentAxes.FontSize = 10;
        
        % Save .fig file
        savefig(['results/fig/',filename]);
        saveas(gcf,['results/png/',filename,'.png'])

        % Plot and save side views
        if (flags.plot_side_views == true)
            pbaspect([10,10,1])
            view(0,90)
            saveas(gcf,['results/png/',filename,'_xy.png'])
            saveas(gcf,['results/pdf/',filename,'_xy.pdf'])
            view(180,0)
            saveas(gcf,['results/png/',filename,'_xz.png'])
            saveas(gcf,['results/pdf/',filename,'_xz.pdf'])
            view(-90,0)
            saveas(gcf,['results/png/',filename,'_yz.png'])
            saveas(gcf,['results/pdf/',filename,'_yz.pdf'])
        end
    end
end