function [path, path_cost] = costEnergy(v_parent, v_child)
%COSTENERGY Summary of this function goes here
%   Detailed explanation goes here

connectionObj = uavDubinsConnection();

connectionObj.AirSpeed = v_parent.state.state.airspeed;
connectionObj.MaxRollAngle = v_parent.state.aircraft_properties.max_roll_angle;
connectionObj.FlightPathAngleLimit = v_parent.state.aircraft_properties.flight_path_angle_limits;

p_init = v_parent.state.state.position;
head_init = v_parent.state.state.heading;
p_goal = v_child.state.state.position;
head_goal = v_child.state.state.heading;
startPose = [p_init head_init]; % [meters, meters, meters, radians]
goalPose = [p_goal head_goal];

[path_segment, ~] = connect(connectionObj,startPose,goalPose);

stepSize = path_segment{1}.Length/100;
lengths = 0:stepSize:path_segment{1}.Length;
poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]

path_no_wind = poses(1:end,1:3); % [x, y, z]
path = path_no_wind;
wind_drift = [0 0 0];
for i=1:size(path,1)
    path(i,:) = path(i,:) + wind_drift;
    [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3));
    wind_drift = wind_drift + [u,v,w]  * stepSize/v_parent.state.state.airspeed;
end

gamma = poses(1:end,5);
roll = poses(1:end,6);
dt = lengths(2)/v_parent.state.state.airspeed;

% solar energy obtained on path
SE = sum(solar_power(path(:,3)) * dt);
% thrust energy spent on path
TE = sum(thrust(v_child, path, gamma, roll) * v_child.state.state.airspeed * dt) / ...
    (v_child.state.aircraft_properties.eff_prop * v_child.state.aircraft_properties.eff_shaft);
% change in potential energy
dPE =  v_child.state.state.potential_energy -  v_parent.state.state.potential_energy;
% change in potential energy
dKE =  v_child.state.state.kinetic_energy -  v_parent.state.state.kinetic_energy;

dEtot = dPE + dKE - TE + SE;
path_cost = -dEtot;

end