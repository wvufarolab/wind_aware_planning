function [cost] = costApproxDubinsAirplane(v_parent, v_child)
%COSTEUCLIDEAN Summary of this function goes here
%   Detailed explanation goes here

offset = (4 + 2*pi)*1/cos(v_parent.state.aircraft_properties.max_roll_angle);
euclidean = norm(v_parent.state.state.position - v_child.state.state.position);
from_z = abs(v_parent.state.state.altitude - v_child.state.state.altitude)/sin(v_parent.state.aircraft_properties.max_roll_angle);
cost = max(euclidean, from_z);

end