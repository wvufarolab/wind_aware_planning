function [v_min, v_new] = chooseParent(V_near, v_nearest, v_new, method)

v_test = exactSteerDubinsAirplane(v_nearest, v_new, method);
c_min = v_test.cost_from_start;

obstacle_free = true;

if ~isempty(V_near)
    id_min = 1;
    for i=1:length(V_near)
        v_near = V_near(i);
        v_test = exactSteerDubinsAirplane(v_near, v_new, method);
        if (obstacle_free == true) % add second condition
            c_prime = v_test.cost_from_start;
            if (c_prime < c_min)
                id_min = i;
                c_min = c_prime;
            end
        end
    end

    v_min = V_near(id_min);
    v_new = exactSteerDubinsAirplane(v_min, v_new, method);
else
    v_min = v_nearest;
    v_new = v_test;
end

end