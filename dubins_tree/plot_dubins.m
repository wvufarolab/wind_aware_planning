%% Add plotting tools
for j=2:S.num_elements
    v = S.get_element(j);
    p_init = v.state.state.position;
    v_parent = S.get_element(v.parent_idx);
    p_goal = v_parent.state.state.position;
    plot3([p_init(1) p_goal(1)], ...
        [p_init(2), p_goal(2)], ...
        [p_init(3),p_goal(3)], 'k-');
    hold on;
end

%%
d = zeros(1,S.num_elements);
for i=1:S.num_elements
    v = S.get_element(i);
    d(i) = norm(v.state.state.position - goal);
end

[d_min, id] = min(d);

v_end = S.get_element(id);
plot3(v_end.traj_from_start(:,1),v_end.traj_from_start(:,2),v_end.traj_from_start(:,3),'r-','LineWidth',3);
hold on;

figure
plot(d)