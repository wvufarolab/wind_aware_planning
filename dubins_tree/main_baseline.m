%% VAMP Path Planning using Airplane Dubins Paths
clear
close all

addpath('./dubins_tree');
addpath('./graph');
addpath('./models');
addpath('./setup');
addpath('./results');

%% Hyperparameters
hps.wind_type = 'venus';
hps.winds       = [90];
hps.speeds      = [30];
hps.step_sizes  = [1e4];
hps.starts = [-5e4, 0e4, 5.5e4, 0];
hps.goals = [3e5, 8e4, 5.5e4, 0];
hps.n_samples = 5000;
hps.max_radius = 1e5;
hps.d_thres = 1e3;

wind_type = hps.wind_type;
wind_magnitude = hps.winds(1);
max_airspeed = hps.speeds(1);
step_size = hps.step_sizes(1);
start = hps.starts(1,:);
goal = hps.goals(1,:);

%% Setup problem and aircraft
environment_setup;
aircraft_setup;

%% Iterate

connectionObj = uavDubinsConnection();
connectionObj.AirSpeed = aircraft_properties.max_airspeed;
connectionObj.MaxRollAngle = aircraft_properties.max_roll_angle;
connectionObj.FlightPathAngleLimit = aircraft_properties.flight_path_angle_limits;

startPose = start; % [meters, meters, meters, radians]
goalPose = goal;

[path_segment, path_length] = connect(connectionObj,startPose,goalPose);
stepSize = path_segment{1}.Length/100;
lengths = 0:stepSize:path_segment{1}.Length;
poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
path_no_wind = poses(1:end,1:3); % [x, y, z]

f = figure;
plot_baseline;

plot3(path_no_wind(:,1),path_no_wind(:,2),path_no_wind(:,3),'b')

path = path_no_wind;
wind_drift = [0 0 0];
for i=1:size(path,1)
    path(i,:) = path(i,:) + wind_drift;
    [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), wind_type, wind_magnitude);
    wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
end
% plot3(path(:,1),path(:,2),path(:,3),'r')

%%
dx = 4e3;
% dy = 1e3;
goal_virtual = goal;
alpha = 1e3;

hold on
while norm(path(end,:) - goal(1:3)) > dx

    % +dx
    goalPose = goal_virtual + [dx,0,0,0];
    [path_segment, path_length] = connect(connectionObj,startPose,goalPose);
    stepSize = path_segment{1}.Length/100;
    lengths = 0:stepSize:path_segment{1}.Length;
    poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
    path_no_wind = poses(1:end,1:3); % [x, y, z]
    path = path_no_wind;
    wind_drift = [0 0 0];
    for i=1:size(path,1)
        path(i,:) = path(i,:) + wind_drift;
        [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), wind_type, wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
    end
    dist_plus =  norm(path(end,:) - goal(1:3));
    
    % -dx
    goalPose = goal_virtual - [dx,0,0,0];
    [path_segment, path_length] = connect(connectionObj,startPose,goalPose);
    stepSize = path_segment{1}.Length/100;
    lengths = 0:stepSize:path_segment{1}.Length;
    poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
    path_no_wind = poses(1:end,1:3); % [x, y, z]
    path = path_no_wind;
    wind_drift = [0 0 0];
    for i=1:size(path,1)
        path(i,:) = path(i,:) + wind_drift;
        [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), wind_type, wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
    end
    dist_neg = norm(path(end,:) - goal(1:3));
    
    dfdx = (dist_plus - dist_neg) / (2*dx);
    
    goal_virtual(1) = goal_virtual(1) - alpha*dfdx;
    
    goalPose = goal_virtual;
    [path_segment, path_length] = connect(connectionObj,startPose,goalPose);
    stepSize = path_segment{1}.Length/100;
    lengths = 0:stepSize:path_segment{1}.Length;
    poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
    path_no_wind = poses(1:end,1:3); % [x, y, z]
    path = path_no_wind;
    wind_drift = [0 0 0];
    for i=1:size(path,1)
        path(i,:) = path(i,:) + wind_drift;
        [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), wind_type, wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
    end
    dist = norm(goal(1:3)-path(end,:))
    plot3(path(:,1),path(:,2),path(:,3),'r');
	hold on
end

%     plot3(path(:,1),path(:,2),path(:,3),'r')

%     plot3(path_no_wind(:,1),path_no_wind(:,2),path_no_wind(:,3),'b')

dist = norm(goal(1:3)-path(end,:));
goalPose(1:3) = goal(1:3) - wind_drift;

gamma = poses(1:end,5);
roll = poses(1:end,6);
T = lengths(end)/aircraft_properties.max_airspeed;
dt = lengths(2)/aircraft_properties.max_airspeed;

z_max = planning_parameters.limits(6);
z_goal = planning_parameters.goal(3);

% solar energy obtained on path
SE = sum(solar_intensity(path(:,3)) * aircraft_properties.solar_panel_area * dt);
% solar energy wasted on the path
SE_comp = sum((solar_intensity(z_max) - solar_intensity(path(:,3))) * aircraft_properties.solar_panel_area * dt);

% thrust energy spent on path
TE = sum(thrust(aircraft_properties, path, gamma, roll) * aircraft_properties.max_airspeed * dt) / ...
    (aircraft_properties.efficiency_propeller * aircraft_properties.efficiency_shaft);

% potential energy wasted on the path
% dPE =  v_child.state.state.potential_energy -  v_parent.state.state.potential_energy;

PE_comp =  aircraft_properties.mass * gravity(z_max) * z_max - aircraft_properties.mass * gravity(z_goal) * z_goal;

dBE = -TE + SE;

path_cost = PE_comp + TE + SE_comp;
