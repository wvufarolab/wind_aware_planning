function [path, path_cost] = costEuclidean(v_parent, v_child)
%COSTEUCLIDEAN Summary of this function goes here
%   Detailed explanation goes here

path_cost = norm(v_parent.state.state.position - v_child.state.state.position);
path = [v_parent.state.state.position, v_child.state.state.position];

end