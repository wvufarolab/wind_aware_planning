function v_nearest = nearest(S, v_rand)

min_distance = inf;
p_goal = v_rand.state.state.position;

% Using Euclidean distance for checking the nearest vertex
for i=1:S.num_elements
    p_init = S.get_element(i).state.state.position;
    distance = norm(p_init -p_goal);
    if distance < min_distance
        id = i;
        min_distance = distance;
    end
end


v_nearest = S.get_element(id);

end