classdef uavDubinsConnectionVenus < handle
    %uavDubinsConnectionVenus Dubins path connection for UAVs
    %   The uavDubinsConnectionVenus object stores information for computing 
    %   uavDubinsPathSegments to connect between start and goal poses of a UAV.
    %
    %   A uavDubinsPathSegment that connects two poses as a sequence of motions
    %   in North-East-Down coordinate system, each of which can be:
    %   - Straight                            "S"
    %   - Left turn (counter-clockwise)       "L"
    %   - Right turn (clockwise)              "R"
    %   - Helix left turn (counter-clockwise) "Hl"
    %   - Helix right turn (clockwise)        "Hr"
    %   - No Action                           "N"
    %
    %   Turn direction is defined when viewed from above the UAV. Helical
    %   motions are used to ascend or descend in altitude. The pose is specified 
    %   as [x y z headingAngle], where [x y z] represents the position in 
    %   meters and the heading angle in radians (measured clockwise, North to East). 
    %   The no action segment "N" is only used as a filler at the end when
    %   only three path segments are needed.
    %
    %   CONNECTIONOBJ = uavDubinsConnectionVenus creates an object with default property value.
    %
    %   CONNECTIONOBJ = uavDubinsConnectionVenus("PropertyName", PropertyValue)
    %   specifies properties to set when creating the object. Property name
    %   options are: 'AirSpeed', 'FlightPathAngleLimit', 'MaxRollAngle'
    %   and 'DisabledPathTypes'.
    %
    %   uavDubinsConnectionVenus properties:
    %
    %   AirSpeed             - UAV airspeed 
    %   MaxRollAngle         - Maximum roll angle
    %   FlightPathAngleLimit - Minimum and maximum flight path angles
    %   DisabledPathTypes    - Path types to disable
    %   MinTurningRadius     - Minimum turning radius (read-only)
    %   AllPathTypes         - List of all possible path types
    %
    %   uavDubinsConnectionVenus methods:
    %
    %   connect              - Connect poses with UAV Dubins curves
    %
    %   Example:
    %       % Create an object of uavDubinsConnectionVenus
    %       connectionObj = uavDubinsConnectionVenus;
    %
    %       % Define startPose and goalPose
    %       startPose = [0 0 0 0];
    %       goalPose  = [0 0 20 pi];
    %
    %       % Compute the path segment between poses
    %       [pathSegObj, pathCosts] = connect(connectionObj, startPose, goalPose)
    %
    %       % Sample the poses
    %       [poses] = interpolate(pathSegObj{1},pathSegObj{1}.Length);
    %
    %       % Visualize the path, start, goal and headings
    %       show(pathSegObj{1});
    %
    %   Example:
    %       % To disable a path type
    %       connectionObj.DisabledPathTypes = connectionObj.AllPathTypes(2);
    %
    %       % To disable more than one path type
    %       I = [1 3 5 23];
    %       connectionObj.DisabledPathTypes = connectionObj.AllPathTypes(I);
    %
    %       disPathType{1} = 'LSRN';
    %       disPathType{2} = 'LSRHr';
    %       connectionObj.DisabledPathTypes = disPathType;
    %
    %   See also dubinsPathSegment, dubinsConnection
    
    %   References:
    %
    %   [1] Mark Owen, Randal W. Beard and Timothy W. McLain, 
    %       "Implementing Dubins Airplane Paths on Fixed-Wing UAVs".
    %       In book: Handbook of Unmanned Aerial Vehicles, 2015.
    
    %   Copyright 2019-2020 The MathWorks, Inc.
    
    %#codegen

    properties (SetAccess = private)
        %MinTurningRadius Minimum turning radius (read-only)
        %   Minimum turning radius, specified in meters. This
        %   corresponds to the radius of the circle at maximum
        %   roll angle and a constant airspeed of the UAV.
        MinTurningRadius
    end
    
    properties
        %AirSpeed UAV airspeed
        %   The UAV's airspeed in meters per second
        %
        %   Default: 10 (meter/sec)
        AirSpeed = 10;
        
        %MaxRollAngle Maximum roll angle
        %   The UAV's max roll angle to make the UAV turn left or right
        %   The minimum and maximum values for MaxRollAngle are greater 
        %   than 0 and less than pi/2, respectively.
        %
        %   Default: 0.5 (radians)
        MaxRollAngle = 0.5;
        
        %FlightPathAngleLimit Minimum and maximum flight path angles
        %   FlightPathAngleLimit, corresponds to the maximum flight path angle the UAV
        %   takes to gain or minimum flight path angle to loose the altitude.
        %   Limit between -pi/2 and pi/2 radians.
        %
        %   Default: [-0.5 0.5]
        FlightPathAngleLimit = [-0.5 0.5];
        
        %DisabledPathTypes Path types to disable
        %   UAV Dubins path types to disable
        %
        %   Default: {}
        DisabledPathTypes;
        
    end
    
    properties (Constant)
        %AllPathTypes Al possible path types
        %   All possible path types of uavDubins curve.
        AllPathTypes = {'LSLN' 'LSRN' 'RSLN' 'RSRN' 'RLRN' 'LRLN' ...
            'HlLSL' 'HlLSR' 'HrRSL' 'HrRSR'  'HrRLR' 'HlLRL' ...
            'LSLHl' 'LSRHr' 'RSLHl' 'RSRHr' 'RLRHr' 'LRLHl' ...
            'LRSL' 'LRSR' 'LRLR' 'RLSR' 'RLRL' 'RLSL'...
            'LSRL' 'RSRL' 'LSLR' 'RSLR'};
    end
    
    properties (Constant, Access = {?private, ?uavDubinsPathSegment})
        %MotionTypes motion types for internal use
        % corresponding basic motion for all path types
        MotionTypes = {{'L' 'S' 'L' 'N'} {'L' 'S' 'R' 'N'} {'R' 'S' 'L' 'N'} {'R' 'S' 'R' 'N'} {'R' 'L' 'R' 'N'} {'L' 'R' 'L' 'N'} ...
            {'Hl' 'L' 'S' 'L'} {'Hl' 'L' 'S' 'R'} {'Hr' 'R' 'S' 'L'} {'Hr' 'R' 'S' 'R'} {'Hr' 'R' 'L' 'R'} {'Hl' 'L' 'R' 'L'} ...
            {'L' 'S' 'L' 'Hl'} {'L' 'S' 'R' 'Hr'} {'R' 'S' 'L' 'Hl'} {'R' 'S' 'R' 'Hr'} {'R' 'L' 'R' 'Hr'} {'L' 'R' 'L' 'Hl'} ...
            {'L' 'R' 'S' 'L'} {'L' 'R' 'S' 'R'} {'L' 'R' 'L' 'R'} {'R' 'L' 'S' 'R'} {'R' 'L' 'R' 'L'} {'R' 'L' 'S' 'L'}...
            {'L' 'S' 'R' 'L'} {'R' 'S' 'R' 'L'} {'L' 'S' 'L' 'R'} {'R' 'S' 'L' 'R'}};
        
        %Gravity
        % 8.87m/sec2
        Gravity = 8.87;
        
        %ErrTolerance
        % error tolerance to accept the solution during optimization
        ErrorTolerance = 1.0/100000;
    end   
    
    methods
        
        %------------------------------------------------------------------
        function this = uavDubinsConnectionVenus(varargin)
                        
            parser = robotics.core.internal.NameValueParser(...
                {'AirSpeed', 'MaxRollAngle', 'FlightPathAngleLimit', 'DisabledPathTypes'}, {10, 0.5, [-0.5 0.5], {}});
            parse(parser, varargin{:});
            
            this.DisabledPathTypes = parameterValue(parser, 'DisabledPathTypes');
            this.FlightPathAngleLimit = parameterValue(parser, 'FlightPathAngleLimit');
            this.MaxRollAngle = parameterValue(parser, 'MaxRollAngle');
            this.AirSpeed = parameterValue(parser, 'AirSpeed');
                        
            %update the MinTurning Radius
            updateMinTurningRadius(this);
        end
                
        %------------------------------------------------------------------
        function [pathSegObjs, pathCosts] = connect(obj, startPoses, goalPoses, varargin)
            %CONNECT Connect poses with UAV Dubins curves
            %
            %   [PATHSEGOBJ, PATHCOST] = CONNECT(CONNECTIONOBJ,STARTPOSE,GOALPOSE)
            %   connects the start and goal pose using the specified connection type object.
            %   By default, the path segment with lowest cost is returned.
            %
            %   [PATHSEGOBJS, PATHCOSTS] = CONNECT(UAVDUBCONNOBJ,STARTPOSE,GOALPOSE,
            %   "PathSegments","all") returns all path segment types with their associated
            %   costs as a cell array.
            %
            %   The START and GOAL pose inputs can be any of the following combination:
            %   - single start pose (1x4) and single goal pose (1x4) (one-to-one mapping)
            %   - single start pose (1x4) and multiple goal poses (mx4) (one-to-all mapping)
            %   - multiple start poses (mx4) and single goal pose (1x4) (all-to-one mapping)
            %   - multiple start poses (mx4) and multiple goal poses (mx4) (one-to-one mapping).
            %
            %   The pose is specified as [x y z headingAngle], where [x y z] represents the position
            %   in meters and the heading angle in radians. The pose follow the North-East-Down coordinate system.
            %   
            %   The PATHSEGOBJS is a cell array of uavDubinsPathSegment objects and
            %   the PATHCOSTS is a vector of cost values.
            %
            %   Example:
            %     % Create an object of uavDubinsConnectionVenus
            %     uavDubConnObj = uavDubinsConnectionVenus;
            %
            %     % Define startPose and goalPose
            %     startPose = [0 0 0 pi];
            %     goalPose  = [1 0 0 pi/2];
            %
            %     % Compute the path segment between poses
            %     [pathSegObj, pathCosts] = connect(uavDubConnObj, ...
            %     startPose, goalPose,"PathSegments","all");
            %
            %   See also uavDubinsPathSegment, dubinsConnection, reedsSheppConnection
            
            % validate the input start and goal poses
            validateattributes(startPoses, {'single', 'double'}, ...
                {'real', 'ncols', 4, 'finite'}, 'connect', 'startPose');
            
            validateattributes(goalPoses, {'single', 'double'}, ...
                {'real', 'ncols', 4, 'finite'}, 'connect', 'goalPose');
            
            nStartPoses = size(startPoses, 1);
            nGoalPoses = size(goalPoses, 1);
            
            % validate the size of start & goal poses
            if((nStartPoses > 1 && nGoalPoses > 1 && nStartPoses ~= nGoalPoses))
            coder.internal.errorIf((nStartPoses > 1 && nGoalPoses > 1 && nStartPoses ~= nGoalPoses), ...
                'shared_autonomous:motionModel:InvalidSizeStartAndGoal');
            end
            
            % define names and default values for name-value pairs
            names = {'PathSegments'};
            defaultValues = {'optimal'};
            
            % parse name-value pairs
            parser = matlabshared.autonomous.core.internal.NameValueParser(names, defaultValues);
            parse(parser, varargin{:});
            
            segs = parameterValue(parser, 'PathSegments');
            validateattributes(segs,{'char','string'},{'nonempty','scalartext'},'connect','PathSegments');
            pathSegments = validatestring(segs, {'optimal' 'all'}, 'connect', 'PathSegments');
            
            % Define FlagOptimal
            flagOptimal = 1;
            if(strcmp(pathSegments,'all'))
                flagOptimal = 0;
            end
            
            %Reshape the inputs to be passed in Builtins
            s1 = reshape(startPoses , [1, nStartPoses * 4]);
            g1 = reshape(goalPoses , [1, nGoalPoses * 4]);
            
            %finding indices of disabledpath types to send to Builtins Obj.
            idx = obj.findDisabledIndices(); 
            
            %Call Builtins
            [pathSegObjs , pathCosts] = uav.internal.uavDubins.uavDubinsBuiltins.connect(obj, s1, g1, nStartPoses, nGoalPoses,...
                (flagOptimal == 1), obj.MinTurningRadius, idx);
        end
        
        %------------------------------------------------------------------
        function set.FlightPathAngleLimit(this,angleLimit)
            %set.MaxFlightPathAngle Setter for MaxFlightPathAngle property
            validateattributes(angleLimit,{'double', 'single'},{'nonempty', 'nonnan', 'real', 'vector', ...
                 'finite','<',pi/2,'>',-pi/2, 'nondecreasing','row', 'numel', 2},'','FlightPathAngleLimit');
            this.FlightPathAngleLimit = angleLimit;
        end
        
        %------------------------------------------------------------------
        function set.MaxRollAngle(this, mRollAngle)
            %set.MaxBankAngle Setter for MaxBankAngle property
            validateattributes(mRollAngle,{'double', 'single'},{'nonempty', 'nonnan', 'real', 'scalar', ...
                'positive', 'finite','<',pi/2, 'nonzero'},'','MaxRollAngle');
            this.MaxRollAngle = mRollAngle;
            updateMinTurningRadius(this);
        end
        
        %------------------------------------------------------------------
        function set.AirSpeed(this, airspeed)
            %set.AirSpeed Setter for AirSpeed property
            validateattributes(airspeed,{'double', 'single'},{'nonempty', 'nonnan', 'real', 'scalar', ...
                'positive', 'finite'},'','AirSpeed');
            this.AirSpeed = airspeed;
            updateMinTurningRadius(this);
        end
        
        %------------------------------------------------------------------
        function set.DisabledPathTypes(this, disabledTypes)
            %set.DisabledPathTypes setter to disable specified path types
            if isempty(disabledTypes)
                validateattributes(disabledTypes, {'cell', 'string'},{});
            else
                
                % Ensure that string input is always a cellstr
                if isstring(disabledTypes)
                    disabledTypes = cellstr(disabledTypes);
                end
                
                % keep the string validation for an informative error message
                validateattributes(disabledTypes, {'cell', 'string'}, {'vector'});
                
                % confirm that all cell array inputs are strings
                coder.internal.errorIf(~iscellstr(disabledTypes), ...
                    'shared_autonomous:validation:CellArrayStringError', 'DisabledPathTypes');
                
                for n = 1 : numel(disabledTypes)
                    validatestring(disabledTypes{n}, this.AllPathTypes, ...
                        '', 'DisabledPathTypes');
                    
                    % Convert to uppercase
                    disabledTypes{n} = upper(disabledTypes{n});
                end
            end
            
            % remove duplicates and assign to property
            this.DisabledPathTypes = ...
                matlabshared.planning.internal.validation.uniquePathTypes(...
                disabledTypes, this.AllPathTypes);
        end
        
        %------------------------------------------------------------------
        function cpObj = copy(obj)
            %copy Create a deep copy of uavDubinsConnectionVenus object.
            cpObj = uavDubinsConnectionVenus(...
                'AirSpeed', obj.AirSpeed, ...
                'DisabledPathTypes', obj.DisabledPathTypes, ...
                'FlightPathAngleLimit',obj.FlightPathAngleLimit, ...
                'MaxRollAngle' , obj.MaxRollAngle);
        end
    end
    
    
    methods (Access = private)
        
        function idx = findDisabledIndices(obj)
            %Temporary variable to support codegen
            allPath = obj.AllPathTypes;
          
            %Converting values in all path to hash values 
            hAllPathTypes = double(zeros(1, numel(obj.AllPathTypes)));
            for idAll = 1:numel(obj.AllPathTypes)
                hAllPathTypes(idAll) = matlabshared.planning.internal.getHashValueForCharVec(allPath{idAll});
            end
                
            %Converting values in DisabledPathTypes to hash values.
            disabledPath = obj.DisabledPathTypes;
            hdisabledTypes = zeros(1, numel(obj.DisabledPathTypes));
            if ~isempty(obj.DisabledPathTypes)
                for idDis = 1:numel(obj.DisabledPathTypes)
                    hdisabledTypes(idDis) = ...
                    matlabshared.planning.internal.getHashValueForCharVec(disabledPath{idDis});
                end
            end

            
            %find the valid pathType id's
            [~ , idx] = setdiff(hAllPathTypes, hdisabledTypes , 'stable');
            [~ , idx] = setdiff(1:28, idx , 'stable');
            idx = sort(idx);
            %idx for disabled path types
            idx = idx-1;
        end
        
        
        %------------------------------------------------------------------
        function updateMinTurningRadius(obj)
            %calculate the min turning radius based on airspeed and bank angle
            obj.MinTurningRadius = obj.AirSpeed*obj.AirSpeed/(obj.Gravity * tan(obj.MaxRollAngle));
        end                
                
    end
end
