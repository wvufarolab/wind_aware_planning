function length = trajLength(traj)

length = 0;
for i = 1:size(traj,1)-1
    p2 = traj(i,:);
    p1 = traj(i+1,:);
    d = norm(p2-p1)
    length = length + d;
end

end