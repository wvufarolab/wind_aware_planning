clear
close all
clc

addpath('../graph');
addpath('../models');
%% Map parameters
x_min = -600;
x_max = 600;
dx = 200;
x = x_min:dx:x_max;

y_min = -600;
y_max = 600;
dy = 200;
y = y_min:dy:y_max;

z_min = 0;
z_max = 100000;
dz = 5000;
z = z_min:dz:z_max;
[X,Y,Z] = meshgrid(x,y,z);

% Wind field
[U, V, W] = wind_field(X,Y,Z);

quiver3(X,Y,Z,U,V,W,0.1,'Autoscale','on')
hold on
view(-25,71)

%%

connectionObj = uavDubinsConnection();

connectionObj.AirSpeed = 15;
connectionObj.MaxRollAngle = 0.8;
connectionObj.FlightPathAngleLimit = [-1.47 1.47];

startPose = [0 0 0 0]; % [meters, meters, meters, radians]
goalPose = [ 200 400 70000 pi/2];
[pathSegObj,~] = connect(connectionObj,startPose,goalPose);

show(pathSegObj{1})

[pathSegObj2,~] = connect(connectionObj,goalPose,startPose);

% pathSegObj = uavDubinsPathSegment(start,goal,flightPathAngle,airSpeed,minTurningRadius,helixRadius,motionTypes,motionLengths)
fprintf('Motion Type: %s\n',strjoin(pathSegObj{1}.MotionTypes));
fprintf('Motion Type: %s\n',strjoin(pathSegObj2{1}.MotionTypes));

% imagem vento vs global
% imagem rrtstar com vento mt forte pro lado 
figure
cycle = [pathSegObj2,pathSegObj];
for i=1:2
    show(cycle{i}), hold on
end

stepSize = pathSegObj{1}.Length/100;
lengths = 0:stepSize:pathSegObj{1}.Length;
poses = interpolate(pathSegObj{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
posesTranslation = poses(1:end,1:3); % [x, y, z]
N = size(poses,1);
posesEulRot = [poses(1:end,4)+pi,ones(N,1)*pi,zeros(N,1)]; % [headingAngle + pi, pi, 0]
posesRotation = quaternion(eul2quat(posesEulRot,'ZYX'));

stepSize2 = pathSegObj2{1}.Length/100;
lengths2 = 0:stepSize2:pathSegObj2{1}.Length;
poses2 = interpolate(pathSegObj2{1},lengths2); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
posesTranslation2 = poses2(1:end,1:3); % [x, y, z]
N2 = size(poses2,1);
posesEulRot2 = [poses2(1:end,4)+pi,ones(N2,1)*pi,zeros(N2,1)]; % [headingAngle + pi, pi, 0]
posesRotation2 = quaternion(eul2quat(posesEulRot2,'ZYX'));

cycleTranslation = [posesTranslation; posesTranslation2];
cycleRotation = [posesRotation; posesRotation2];
%%

plotTransforms(cycleTranslation,cycleRotation,'MeshFilePath','fixedwing.stl','MeshColor','cyan')
% hold on
% plot3(cycleTranslation(:,1),cycleTranslation(:,3),cycleTranslation(:,3))

airspeed = 30;
dist = lengths(2);
wind_drift = [0 0 0];
for i=1:size(cycleTranslation,1)
    cycleTranslation(i,:) = cycleTranslation(i,:) + wind_drift;
    wind_drift = wind_drift + wind_field(cycleTranslation(i,1),cycleTranslation(i,2),cycleTranslation(i,3)) * dist/airspeed;
end

% figure(2)
plotTransforms(cycleTranslation,cycleRotation,'MeshFilePath','fixedwing.stl','MeshColor','cyan')
% hold on
% plot3(cycleTranslation(:,1),cycleTranslation(:,3),cycleTranslation(:,3))

dist = @costDubinsAirplane;
