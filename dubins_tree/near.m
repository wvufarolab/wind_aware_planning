function V_near = near(S, v_new, n, max_radius, limits)

% S. Karaman and E. Frazzoli, Sampling-based Algorithms for Optimal Motion 
% Planning, International Journal of Robotics Research, Vol 30, No 7, 2011. 
% https://arxiv.org/abs/1105.1186
% 
% radius = min{γ_RRTStar*(log(card(V))/ card(V))^(1/d), η}
% - card(V) is the cardinality of the vertex set
% 
% Theorem 38 
% If γ_RRTStar > 2*(1+1/d)^(1/d)*(µ(Xfree)/ζd)^(1/d), then the RRT∗ 
% algorithm is asymptotically optimal.
% 
% - d is the dimension of the space X 
% - µ(Xfree) denotes the Lebesgue measure (i.e., volume) of the 
% obstacle-free space
% - ζd is is the volume of the unit ball in the d-dimensional Euclidean 
% space

p_goal = v_new.state.state.position;

xDim = limits(2)- limits(1);
yDim = limits(4)- limits(3);
zDim = limits(6)- limits(5);

dim = 3;
sigma_d = 4/3 * pi; 
mu_Xfree = xDim*yDim*zDim;

gamma_RRTStar = 2*(1+1/dim)^(1/dim)*(mu_Xfree/sigma_d)^(1/dim);
l = min(gamma_RRTStar * (log(n+1)/n)^(1/dim), max_radius);

V_near = [];
for i=1:S.num_elements
    p_init = S.get_element(i).state.state.position;
    distance = norm(p_init-p_goal);
    if (distance < l)
        V_near = [V_near, S.get_element(i)];
    end
end
% length(V_near)

str = ['Size of Near set: ', int2str(length(V_near)),'. Ball radius: ', int2str(l), '.'];
disp(str);

end