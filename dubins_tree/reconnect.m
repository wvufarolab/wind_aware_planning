function [S, V, E] = reconnect(S, V, E, v_new, v_near, v_near_prime)
% v_near
% v_near_prime
% v_new
% S.container(v_near.parent_idx)
% Remove edge from the parent of v_near to v_near
E.remove_edge(v_near.parent_idx, v_near.idx);
cs = S.container(v_near.parent_idx).children_set;
S.container(v_near.parent_idx).children_set = cs(cs~=v_near.idx);

% Overwrite v_parent with v_parent_prime
S.container(v_near.idx) = v_near_prime;

% Add edge from v_parent to v_child
E.add_edge(v_new.idx, v_near_prime.idx);

% Add v_child to the children set of v_parent
S.container(v_new.idx).children_set = [S.container(v_new.idx).children_set, v_near_prime.idx];

end