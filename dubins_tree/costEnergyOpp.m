function [path, path_cost, dBE, total_time] = costEnergyOpp(v_parent, v_child, total_time)
%COSTENERGY Summary of this function goes here
%   Detailed explanation goes here

n = 100;

p_init = v_parent.state.state.position;
head_init = v_parent.state.state.heading;
p_goal = v_child.state.state.position;
head_goal = v_child.state.state.heading;

b = v_parent.state.state.battery_energy;
battery_threshold = 0.1;
if (b < battery_threshold)
    % TODO - Apply descend
    poses = repmat([p_init, head_init, 0, 0],n+1,1); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
    
    dt = total_time/n;
    
    path_no_wind = poses(1:end,1:3); % [x, y, z]
    path = path_no_wind;
    wind_drift = [0 0 0];
    for i=1:size(path,1)
        path(i,:) = path(i,:) + wind_drift;
        [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), v_parent.state.planning_parameters.wind_type, v_parent.state.planning_parameters.wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * dt;
    end
    
    % no thrust energy spent on path
    TE_cum = zeros(n+1,1);
    
    % solar energy obtained on path
    SE_cum = cumsum(solar_intensity(path(:,3)) * v_child.state.aircraft_properties.solar_panel_area * dt);
else
    connectionObj = uavDubinsConnection();
    connectionObj.AirSpeed = v_parent.state.state.airspeed;
    connectionObj.MaxRollAngle = v_parent.state.aircraft_properties.max_roll_angle;
    connectionObj.FlightPathAngleLimit = v_parent.state.aircraft_properties.flight_path_angle_limits;

    startPose = [p_init head_init]; % [meters, meters, meters, radians]
    goalPose = [p_goal head_goal];
    [path_segment, ~] = connect(connectionObj,startPose,goalPose);
    
    stepSize = path_segment{1}.Length/n;
    lengths = 0:stepSize:path_segment{1}.Length;
    poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]

    path_no_wind = poses(1:end,1:3); % [x, y, z]
    path = path_no_wind;
    wind_drift = [0 0 0];
    for i=1:size(path,1)
        path(i,:) = path(i,:) + wind_drift;
        [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), v_parent.state.planning_parameters.wind_type, v_parent.state.planning_parameters.wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * stepSize/v_parent.state.state.airspeed;
    end
    
    total_time = lengths(end)/v_parent.state.state.airspeed;
    dt = lengths(2)/v_parent.state.state.airspeed;
    
    gamma = poses(1:end,5);
    roll = poses(1:end,6);
    
    % thrust energy spent on path
    TE_cum = cumsum(thrust(v_child.state.aircraft_properties, path, gamma, roll) * v_child.state.state.airspeed * dt) / ...
        (v_child.state.aircraft_properties.efficiency_propeller * v_child.state.aircraft_properties.efficiency_shaft);
    
    % solar energy obtained on path
    SE_cum = cumsum(solar_intensity(path(:,3)) * v_child.state.aircraft_properties.solar_panel_area * dt);
    
    % change in battery at each instant
    dBE_cum = -TE_cum + SE_cum;
    b_cum = b + dBE_cum;

    % check if battery < 0 happens
    for i=1:size(b_cum,1)
        if(b_cum(i) < battery_threshold)
        
%             % recalculate drift
%             wind_drift = [0 0 0];
%             for j=i:size(path,1)
%                 path(j,:) = path(j,:) + wind_drift;
%                 [u,v,w] = wind_field(path(j,1),path(j,2),path(j,3), v_parent.state.planning_parameters.wind_type, v_parent.state.planning_parameters.wind_magnitude);
%                 wind_drift = wind_drift + [u,v,w]  * stepSize/v_parent.state.state.airspeed;
%             end
%             % no thrust energy spent on the rest of the path
%             TE_cum(i:end) = TE_cum(i);
%             % solar energy obtained on path
%             SE_cum = cumsum(solar_intensity(path(:,3)) * v_child.state.aircraft_properties.solar_panel_area * dt);
            
            path = path(1:i,:);
            TE_cum = TE_cum(1:i);
            SE_cum = SE_cum(1:i);
            total_time = i/size(b_cum,1) * total_time;
            break
        end
    end
end

TE = TE_cum(end);
SE = SE_cum(end);

% Maximum altitude
z_max = v_child.state.planning_parameters.limits(6);

% Solar energy wasted on the path
SE_comp = sum((solar_intensity(z_max) - solar_intensity(path(:,3))) * v_child.state.aircraft_properties.solar_panel_area * dt);

% Potential energy wasted on the path
% dPE =  v_child.state.state.potential_energy -  v_parent.state.state.potential_energy;
PE_comp =  v_child.state.aircraft_properties.mass * gravity(z_max) * z_max - v_child.state.state.potential_energy;

dBE = -TE + SE;

path_cost = PE_comp + TE + SE_comp;

% disp(['Altitude gain: ', num2str(v_child.state.state.altitude -  v_parent.state.state.altitude), ...
%     '. Total time: ', num2str(T), ...
%     '. Thrust: ', num2str(mean(thrust(v_child, path, gamma, roll))), ...
%     '. Propulsive energy: ', num2str(TE), ...
%     '. Solar energy: ', num2str(SE), ...
%     '. Potential energy: ', num2str(dPE), ...
%     '. Total energy: ', num2str(dEtot), '.']);
end