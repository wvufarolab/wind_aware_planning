function v_child = exactSteerZeroBattery(v_parent, v_child, method, total_time)

switch(method)
    case 'dubinsZB'
        [path, path_cost, dBE, T] = costDubinsAirplane(v_parent, v_child, total_time);
    case 'energySpentZB'
        [path, path_cost, dBE, T] = costEnergySpent(v_parent, v_child, total_time);
    case 'energyOppZB'
        [path, path_cost, dBE, T] = costEnergyOpp(v_parent, v_child, total_time);
end

v_child.state.state.position = path(end, :);
v_child.state.state.altitude = v_child.state.getAltitude();
v_child.state.state.kinetic_energy = v_child.state.getKineticEnergy();
v_child.state.state.potential_energy = v_child.state.getPotentialEnergy();

if (v_parent.state.state.battery_energy + dBE > v_parent.state.aircraft_properties.battery_max_energy) 
    v_child.state.state.battery_energy = v_parent.state.aircraft_properties.battery_max_energy;
elseif (v_parent.state.state.battery_energy + dBE < v_parent.state.aircraft_properties.battery_min_energy)
    v_child.state.state.battery_energy = 0;
%     path_cost = Inf;
else
    v_child.state.state.battery_energy = v_parent.state.state.battery_energy + dBE;
end

v_child.parent_idx = v_parent.idx;
v_child.cost_from_parent = path_cost;
v_child.cost_from_start = v_parent.cost_from_start + path_cost;
v_child.traj_from_parent = path;
v_child.traj_from_start = [v_parent.traj_from_start; path];
v_child.state.state.time = v_parent.state.state.time + T;

end
