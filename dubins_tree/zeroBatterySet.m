function V_zero = zeroBatterySet(S)

b_thres = 0.1;
V_zero = [];
for i=1:S.num_elements
    b = S.get_element(i).state.state.battery_energy;
    if (b < b_thres)
        V_zero = [V_zero, S.get_element(i)];
    end
end
% length(V_near)

str = ['Size of Zero Battery set: ', int2str(length(V_zero)), '.'];
disp(str);

end