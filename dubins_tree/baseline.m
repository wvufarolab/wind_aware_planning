%% VAMP Path Planning using Airplane Dubins Paths
clear
close all

addpath('../graph');
addpath('../models');

problem_setup;
aircraft_setup;

%% Plot the problem setup

plot3(initial_state.position(:,1),initial_state.position(:,2),initial_state.position(:,3),'g.','MarkerSize',10);
text(initial_state.position(:,1),initial_state.position(:,2),initial_state.position(:,3),['Start:',num2str(initial_state.position)]);

plot3(goal(:,1),goal(:,2),goal(:,3),'r.','MarkerSize',10);
text(goal(:,1),goal(:,2),goal(:,3),['Goal:',num2str(goal)]);
hold on;

%% Iterate

connectionObj = uavDubinsConnection();
connectionObj.AirSpeed = aircraft_properties.max_airspeed;
connectionObj.MaxRollAngle = 0.8;
connectionObj.FlightPathAngleLimit = [-1.47 1.47];

p_init = start;
head_init = 0;
p_goal = goal;
head_goal = 0;

startPose = [p_init head_init]; % [meters, meters, meters, radians]
goalPose = [p_goal head_goal];

dist = [inf];
N = 1;
% while (dist(end) > 1000)plot3(path_no_wind(:,1),path_no_wind(:,2),path_no_wind(:,3),'b')

[path_segment, path_length] = connect(connectionObj,startPose,goalPose);
stepSize = path_segment{1}.Length/100;
lengths = 0:stepSize:path_segment{1}.Length;
poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
path_no_wind = poses(1:end,1:3); % [x, y, z]
plot3(path_no_wind(:,1),path_no_wind(:,2),path_no_wind(:,3),'b')

%%
p_goal = [5.2e3, 1e4, 6e4];

startPose = [p_init head_init]; % [meters, meters, meters, radians]
goalPose = [p_goal head_goal];

[path_segment, path_length] = connect(connectionObj,startPose,goalPose);
stepSize = path_segment{1}.Length/100;

lengths = 0:stepSize:path_segment{1}.Length;
poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
path_no_wind = poses(1:end,1:3); % [x, y, z]

path = path_no_wind;
wind_drift = [0 0 0];
for i=1:size(path,1)
    path(i,:) = path(i,:) + wind_drift;
    [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3));
    wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
end

dist = norm(goal-path(end,:));
plot3(path(:,1),path(:,2),path(:,3),'r')
