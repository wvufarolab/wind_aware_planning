%% VAMP Path Planning using Airplane Dubins Paths
clear
close all

addpath('./dubins_tree');
addpath('./graph');
addpath('./models');
addpath('./setup');

%% Hyperparameters
wind_magnitude = 0;
wind_type = 'constant';

max_airspeed = 30;

n_samples_rrtStar = 400;
step_size = 1e4;
max_radius = 1e5;

start = [0, 0, 0, 0];
goal = [4e2, 6e2, 1e3, 0];

%% Setup problem and aircraft
problem_setup_drift;
aircraft_setup;
 
%%

connectionObj = uavDubinsConnection();
connectionObj.AirSpeed = aircraft_properties.max_airspeed;
connectionObj.MaxRollAngle = aircraft_properties.max_roll_angle;
connectionObj.FlightPathAngleLimit = aircraft_properties.flight_path_angle_limits;

startPose = start; % [meters, meters, meters, radians]
goalPose = goal;

[pathSegObj,~] = connect(connectionObj,startPose,goalPose);

% show(pathSegObj{1},'Positions',{})

fprintf('Motion Type: %s\n',strjoin(pathSegObj{1}.MotionTypes));

stepSize = pathSegObj{1}.Length/10;
lengths = 0:stepSize:pathSegObj{1}.Length;
poses = interpolate(pathSegObj{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
posesTranslation = poses(1:end,1:3); % [x, y, z]
N = size(poses,1);
posesEulRot = [poses(1:end,4)+pi,ones(N,1)*pi,zeros(N,1)]; % [headingAngle + pi, pi, 0]
posesRotation = quaternion(eul2quat(posesEulRot,'ZYX'));

hold on
% plotTransforms(posesTranslation,posesRotation,'FrameSize',20,'MeshFilePath','fixedwing.stl','MeshColor','red')

stepSize = pathSegObj{1}.Length/1000;
lengths = 0:stepSize:pathSegObj{1}.Length;
poses = interpolate(pathSegObj{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]
posesTranslation = poses(1:end,1:3); % [x, y, z]
N = size(poses,1);
posesEulRot = [poses(1:end,4)+pi,ones(N,1)*pi,zeros(N,1)]; % [headingAngle + pi, pi, 0]
posesRotation = quaternion(eul2quat(posesEulRot,'ZYX'));

dist = lengths(2);
for u = -15:5:15
    wind_magnitude = u;
    pT = posesTranslation;
    wind_drift = [0 0 0];
    for i=1:size(pT,1)
        pT(i,:) = pT(i,:) + wind_drift;
        [u,v,w] = wind_field(pT(i,1),pT(i,2),pT(i,3), wind_type, wind_magnitude);
        wind_drift = wind_drift + [u,v,w]  * stepSize/aircraft_properties.max_airspeed;
    end
    
    hold on
    if u>0
        plot3(pT(:,1),pT(:,2),pT(:,3),'Color',[u 0 0]/15)
    elseif u<0
        plot3(pT(:,1),pT(:,2),pT(:,3),'Color',[0 0 -u]/15)
    else
        plot3(pT(:,1),pT(:,2),pT(:,3),'Color',[0 0 0])
    end
end

axis equal
view(2)
legend('Start','Goal','-15 m/s','-10 m/s','-5 m/s','0 m/s','5 m/s','10 m/s', '15 m/s','Location','best')
axis(limits)
grid on