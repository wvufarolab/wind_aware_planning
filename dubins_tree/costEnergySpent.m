function [path, path_cost, dBE, T] = costEnergySpent(v_parent, v_child)
%COSTENERGY Summary of this function goes here
%   Detailed explanation goes here

connectionObj = uavDubinsConnection();

connectionObj.AirSpeed = v_parent.state.state.airspeed;
connectionObj.MaxRollAngle = v_parent.state.aircraft_properties.max_roll_angle;
connectionObj.FlightPathAngleLimit = v_parent.state.aircraft_properties.flight_path_angle_limits;

p_init = v_parent.state.state.position;
head_init = v_parent.state.state.heading;
p_goal = v_child.state.state.position;
head_goal = v_child.state.state.heading;
startPose = [p_init head_init]; % [meters, meters, meters, radians]
goalPose = [p_goal head_goal];

[path_segment, ~] = connect(connectionObj,startPose,goalPose);

stepSize = path_segment{1}.Length/100;
lengths = 0:stepSize:path_segment{1}.Length;
poses = interpolate(path_segment{1},lengths); % [x, y, z, headingAngle, flightPathAngle, rollAngle]

path_no_wind = poses(1:end,1:3); % [x, y, z]
path = path_no_wind;
wind_drift = [0 0 0];
for i=1:size(path,1)
    path(i,:) = path(i,:) + wind_drift;
    [u,v,w] = wind_field(path(i,1),path(i,2),path(i,3), v_parent.state.planning_parameters.wind_type, v_parent.state.planning_parameters.wind_magnitude);
    wind_drift = wind_drift + [u,v,w]  * stepSize/v_parent.state.state.airspeed;
end

gamma = poses(1:end,5);
roll = poses(1:end,6);
T = lengths(end)/v_parent.state.state.airspeed;
dt = lengths(2)/v_parent.state.state.airspeed;

% solar energy obtained on path
SE = sum(solar_intensity(path(:,3)) * v_child.state.aircraft_properties.solar_panel_area * dt);
% thrust energy spent on path
TE = sum(thrust(v_child.state.aircraft_properties, path, gamma, roll) * v_child.state.state.airspeed * dt) / ...
    (v_child.state.aircraft_properties.efficiency_propeller * v_child.state.aircraft_properties.efficiency_shaft);

dBE = -TE + SE;

path_cost = TE;

% disp(['Altitude gain: ', num2str(v_child.state.state.altitude -  v_parent.state.state.altitude), ...
%     '. Total time: ', num2str(T), ...
%     '. Thrust: ', num2str(mean(thrust(v_child, path, gamma, roll))), ...
%     '. Propulsive energy: ', num2str(TE), ...
%     '. Solar energy: ', num2str(SE), ...
%     '. Potential energy: ', num2str(dPE), ...
%     '. Total energy: ', num2str(dEtot), '.']);
end