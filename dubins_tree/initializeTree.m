function [S, V, E] = initializeTree()

% Create the container for all nodes
S = container_set(vertex.empty());

% Create the node set - This contains only indexes for the nodes. The
% actual nodes are stored in the container set
V = vertex_set();

% Create de edge set
E = edge_set();

end