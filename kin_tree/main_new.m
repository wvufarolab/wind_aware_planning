 %% VAMP Path Planning using Airplane Dubins Paths
clear
close all

addpath('../graph');
addpath('../models');

%% Map parameters

x_min = -5e4; % ~ -50km longitude / 
x_max = 5e4; % ~ +50km longitude
% x_min = -1.7e7; % ~ +180deg longitude / 
% x_max = 1.7e7; % ~ +180deg longitude
x = linspace(x_min,x_max,10);

y_min = -5e4; % -50km latitude
y_max = 5e4; % +50km latitude
% y_min = 4.0e6; % ~ -20deg latitude
% y_max = 4.0e6; % ~ +20deg latitude
y = linspace(y_min,y_max,10);

z_min = 5.5e4; % ~ 55km altitude, 100% buoyant
z_max = 7.0e4; % ~ 55km altitude, 100% buoyant
z = linspace(z_min,z_max,10);

limits = [x_min, x_max, y_min, y_max, z_min, z_max];

%% Create Aircraft object

aircraft_properties.mass = 4.5e2;
aircraft_properties.volume = aircraft_properties.mass / air_density(z_min);

aircraft_properties.wing_chord = 10;
aircraft_properties.wing_span = 50;
aircraft_properties.wing_area = aircraft_properties.wing_chord * aircraft_properties.wing_span;
aircraft_properties.wing_aspect_ratio = aircraft_properties.wing_span ^2 / aircraft_properties.wing_area;
aircraft_properties.wing_efficiency = 0.85;
aircraft_properties.ci = [0.02, 0, 1/(pi* aircraft_properties.wing_aspect_ratio * aircraft_properties.wing_efficiency)];
% aircraft_properties.ci = [0.0194, -0.0624, 0.2397, -0.3164, 0.1723];
aircraft_properties.max_roll_angle = deg2rad(30);
aircraft_properties.flight_path_angle_limits = deg2rad([-45, 45]);

initial_state.altitude = 60000;
initial_state.position = [0, 0, initial_state.altitude];
initial_state.heading = 0;
initial_state.airspeed = 30;
initial_state.time = 0;
initial_state.battery = 0;
initial_state.kinetic_energy = 0;
initial_state.potential_energy = 0;

environment_properties.air_density = air_density(initial_state.altitude);
environment_properties.gravity = gravity(initial_state.altitude);

planning_parameters.integration_time = 120;
planning_parameters.input_T = [0, 10];
planning_parameters.input_airspeed = [20, 30];
planning_parameters.input_delta_heading = deg2rad(-50:10:50);
planning_parameters.input_bank_angle =  deg2rad([0, 30]);

vamp = aircraft(aircraft_properties, initial_state, environment_properties, planning_parameters);



