%% VAMP Path Planning using Airplane Dubins Paths
clear
close all

addpath('../graph');
addpath('../models');

problem_setup;
aircraft_setup;

%% Plot the problem setup

plot3(initial_state.position(:,1),initial_state.position(:,2),initial_state.position(:,3),'g.','MarkerSize',10);
text(initial_state.position(:,1),initial_state.position(:,2),initial_state.position(:,3),['Start:',num2str(initial_state.position)]);

plot3(goal(:,1),goal(:,2),goal(:,3),'r.','MarkerSize',10);
text(goal(:,1),goal(:,2),goal(:,3),['Goal:',num2str(goal)]);
hold on;


%% KT 

% Initialize graph
[S, V, E] = initializeTree();

% Create first Vertex object
v_init = vertex(S.get_next_idx(), vamp, 0, 0, 0, [], [], []);

[S, V, E] = insertNode(S, V, E, [], v_init);
%%
%Create a graph
for N=1:2
    
    min_cost = inf;
    % Generate a new node with minimum cost
    for i=1:length(vamp.planning_parameters.input_T)
        for j=1:length(vamp.planning_parameters.input_airspeed)
            for k=1:length(vamp.planning_parameters.input_delta_heading)
                for l=1:length(vamp.planning_parameters.input_bank_angle)
                    input.T = vamp.planning_parameters.input_T(i);
                    input.airspeed = vamp.planning_parameters.input_airspeed(j);
                    input.delta_heading = vamp.planning_parameters.input_delta_heading(k);
                    input.bank_angle = vamp.planning_parameters.input_bank_angle(l);
                    [~, ~, cost] = vamp.getNextState(input, 1, goal, true);
                    if cost < min_cost
                        best_input = input;
                    end
                end
            end
        end
    end

    [vamp, traj_from_parent, cost_from_parent] = vamp.getNextState(best_input, 1, goal, true);

    traj_from_start = [v_start.traj_from_start; traj_from_parent];

    cost_from_start = v_start.cost_from_start + cost_from_parent;

    v_new = vertex(S.get_next_idx(), vamp, v_start.idx, cost_from_parent, cost_from_start, [], traj_from_parent, traj_from_start);
       
    S.add_element(v_new);

    V.add_vertex(v_new.idx);

    E.add_edge(v_start.idx, v_new.idx);

    S.container(v_start.idx).children_set = v_new.idx;
    
    v_start = v_new;
end

%% 

plot_kin_tree;
