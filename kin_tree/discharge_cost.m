function cost = discharge_cost(height_trajectory, thrust, total_time)
%DISCHARGE_COST Summary of this function goes here
%   Detailed explanation goes here

% Energy consumed by the propulsion system
KT = 0.0250/0.6;
power = thrust * KT;
propulsion_discharge = power * total_time;

% Energy harvested by the solar panels system

dt = diff(height_trajectory(1,:));
solar_charge = 0;
for i=1:length(height_trajectory(1,:))-1
    mean_height = (height_trajectory(2,i) + height_trajectory(2,i+1))/2;
    solar_charge = solar_panel_model(mean_height) * dt(i);
end

cost = propulsion_discharge - solar_charge;

end