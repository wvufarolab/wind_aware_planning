function cost = total_cost(kinematic_cost, discharge_cost, proximity_heuristic)
%KINEMATIC_COST Summary of this function goes here
%   Detailed explanation goes here
cost = (kinematic_cost + discharge_cost) / proximity_heuristic;
end