t = 120;
goal = [5000 5000 3000];
%%
% Initial condition
pos_i = [0 0 1500];
v_i = 20;
T_i = 0;

%%
% Case 1- Steady flight, no thrust
pos_1 = [500 500 1200];
v_1 = 20;
T_1 = 0;
kcost_1 = kinematic_cost(pos_i(3), pos_1(3), v_i, v_1);
pcost_1 = proximity_heuristic(pos_1, goal);
h_traj1 = [linspace(0, t, 100); linspace(pos_i(3), pos_1(3), 100)];
dcost_1 = discharge_cost(h_traj1, T_1, t);

cost_1 = total_cost(kcost_1, dcost_1, pcost_1);
%%
% Case 2- Steady flight, trade vel for height
pos_2 = [500 500 (1607.1875-300)];
v_2 = 15;
T_2 = 0;
kcost_2 = kinematic_cost(pos_i(3), pos_2(3), v_i, v_2);
pcost_2 = proximity_heuristic(pos_2, goal);
h_traj2 = [linspace(0, t, 100); linspace(pos_i(3), pos_2(3), 100)];
dcost_2 = discharge_cost(h_traj2, T_2, t);

cost_2 = total_cost(kcost_2, dcost_2, pcost_2);
%%
% Case 3- Steady flight, with thrust
pos_3 = [500 500 1800];
v_3 = 20;
T_3 = 100;
kcost_3 = kinematic_cost(pos_i(3), pos_3(3), v_i, v_3);
pcost_3 = proximity_heuristic(pos_3, goal);9.
h_traj3 = [linspace(0, t, 100); linspace(pos_i(3), pos_3(3), 100)];
dcost_3 = discharge_cost(h_traj3, T_3, t);

cost_3 = total_cost(kcost_3, dcost_3, pcost_3);
