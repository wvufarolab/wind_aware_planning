function cost = kinematic_cost(h1, h2, V1, V2)
%KINEMATIC_COST Summary of this function goes here
%   Detailed explanation goes here
rho = 1.225;
cost = (h2-h1) + 0.5*rho*(V2^2 - V1^2);

end