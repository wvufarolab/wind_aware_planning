% Map parameters
x = 0:1:60;
y = 0:1:60;
z = 0:100:6000;
[X,Y,Z] = meshgrid(x,y,z);

% Wind field
[U, V, W] = wind_field(X,Y,Z);
quiver3(X,Y,Z,U,V,W)

vamp = aircraft([0,0,1000],0,20,1000,0)

vamp.plotPrimitives