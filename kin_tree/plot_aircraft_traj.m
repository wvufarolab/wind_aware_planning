%% VAMP Path Planning using Kinodynamic Tree
clc
clear
close all

addpath('graph');
addpath('wind_model');

%% Map parameters
x = -250:10:250;
y = -250:10:250;
z = 0:10:500;
[X,Y,Z] = meshgrid(x,y,z);

% Wind field
[U, V, W] = wind_field(X,Y,Z);
quiver3(X,Y,Z,U,V,W,0.1,'Autoscale','on')
hold on

%%
frames = 300; % number of frames in our movie show. 

x3 = zeros(1,300); 
x3(201:273) = -80*sind((0:15:3*360)-90)-80;
y3 = zeros(1,300);  
y3(1:200) = linspace(-250,0,200);
y3(201:273) = 80*cosd((0:15:3*360)-90);
y3(274:end) = linspace(0,250,length(y3)-273); 
z3 = 120+40*cos(linspace(pi,4*pi,length(y3)))+y3/4;
path3 = plot3(x3,y3,z3,'b.-');
hold on
% Set view: 
% view([70 33])
axis([-250 250 -250 250 0 500])
grid off
set(gca,'color','none')
axis off    

[roll3,pitch3,yaw3] = xyz2rpy(x3,y3,z3,30);

az = linspace(70,85,frames); 
el = linspace(33,29,frames); 
% delete(path3)

for k = 1:frames
    h3 = c130(x3(k),y3(k),z3(k),...
         'lines','none',...
         'wings','b',...
         'tail','r',...
         'roll',roll3(k),...
         'pitch',pitch3(k),...
         'yaw',yaw3(k));
     % set view and take a picture:
%      view([az(k) el(k)])
     axis([-250 250 -250 250 0 500])
%      frame = export_fig(gcf,'-nocrop','-m2');
%      writeVideo(writerObj,frame);
      pause(0.01);
%       pause;

     % delete the planes we just drew: 
     delete(h3); 
 end