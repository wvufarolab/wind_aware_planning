%%

frames = 242; % number of frames in our movie show. 

figure 
plot3(traj_from_start(1:frames,1),traj_from_start(1:frames,2),traj_from_start(1:frames,3));
h = gcf
h.WindowState = 'fullscreen'
%
axis([-1500 1500 -1500 1500 70000 90000])
view([0 45])
grid on

[r,p,y] = xyz2rpy(traj_from_start(1:frames,1),traj_from_start(1:frames,2),traj_from_start(1:frames,3),30);
hold on
for k = 1:frames
    h3 = c130(traj_from_start(k,1),traj_from_start(k,2),traj_from_start(k,3),...
         'lines','none',...
         'wings','b',...
         'tail','r',...
         'roll',r(k),...
         'pitch',p(k),...
         'yaw',y(k));


     % set view and take a picture:
%      view([az(k) el(k)])
%      frame = export_fig(gcf,'-nocrop','-m2');
%      writeVideo(writerObj,frame);
      pause(0.01);
%       pause;

     % delete the planes we just drew: 
     delete(h3); 
 end
