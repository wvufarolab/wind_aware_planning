n=2000;
L=300;
ds=L/n;
s=(1:n)*ds;

x=1;
y=2;
X=x;
Y=y;
gamma=1;
A=20;
Phi0=pi/8;
for i=1:n
    x=x+ds*cos(gamma *s(i)^2/(2*A^2) + Phi0);
    y=y+ds*sin(gamma *s(i)^2/(2*A^2) + Phi0);
    X=[X x];
    Y=[Y y];
end
plot(X,Y);
dX = diff(X);
dY = diff(Y);
ddX = diff(dX);
ddY = diff(dY);

k= (dX(2:end).*ddY-dY(2:end).*ddX)./(dX(2:end).^2+dY(2:end).^2).^(3/2);
rc = 1./k;
epsilon = 0.005;
abs(A^2 - rc(end)*L) < A^2*epsilon;
%%
XC = [1,1];
XF = [3,1];

v = [cos(15*pi/8), sin(15*pi/8)];

CF = XF-XC;
R = hypot(CF(1),CF(2));

gamma = -1;

vt = gamma*[CF(2),CF(1)];

Phi0 = atan2(v(2),v(1));
alpha = atan2(vt(2),vt(1));

if gamma*(alpha-Phi0) >= 0
    tau = gamma*(alpha-Phi0);
else
    tau = gamma*(alpha-Phi0) + 2*pi;
end

L = 2*R*tau;
A = sqrt(R*L);

C = circle(XC,R);
figure
hold on
plot(C(1,:),C(2,:),'--');
axis equal
xlim([-3,8]);
ylim([-1,5]);

n=500;
ds=L/n;
% s=(n:-1:1)*ds;
s=(1:n)*ds;

x=XF(1);
y=XF(2);
X=x;
Y=y;
for i=n:-1:1
    x=x-ds*cos(gamma *s(i)^2/(2*A^2) + Phi0);
    y=y-ds*sin(gamma *s(i)^2/(2*A^2) + Phi0);
    X=[X x];
    Y=[Y y];
end
plot(X,Y);

%%

